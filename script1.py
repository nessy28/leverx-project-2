import sys


def sum(var1: str, var2: str) -> int:
    try:
        var1, var2 = map(int, [var1, var2])
        sumVars = var1 + var2
        return (sumVars)
    except:
        return None


def final(outputMessage: str):
    import sys

    original_stdout = sys.stdout  # Save a reference to the original standard output

    with open('outputMessage.txt', 'w') as f:
        sys.stdout = f  # Change the standard output to the file we created.
        print(f'{outputMessage}')
        sys.stdout = original_stdout  # Reset the standard output to its original value


if __name__ == "__main__":
    try:
        var1 = sys.argv[1]
        var2 = sys.argv[2]
        result = sum(var1, var2)
        if result != None:
            final(f"Сумма чисел: {result}")
        else:
            final(f"Произошла ошибка :(")
    except IndexError:
        final('Невалидные данные')
