import sys

def sum (var1:str, var2:str) -> int:
    var1, var2 = map (int, [var1, var2])
    sumVars = var1 + var2
    return (sumVars) 
    

if __name__ == "__main__":
    var1 = sys.argv[1]
    var2 = sys.argv[2]
    print (f"Сумма чисел: {sum (var1, var2)}")